package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        try {
            LinkedList<Double> st = new LinkedList<>();
            LinkedList<Character> op = new LinkedList<Character>();
            DecimalFormat df = new DecimalFormat("##.####");
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (isDelim(c))
                    continue;
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        processOperator(st, op.removeLast());
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        processOperator(st, op.removeLast());
                    op.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand += statement.charAt(i++);
                    }
                    i--;
                    st.add(Double.parseDouble(operand));
                }
            }
            while (!op.isEmpty())
                processOperator(st, op.removeLast());

                if(Double.isInfinite(st.get(0))){
                    return null;
                }else{
                    return df.format(st.get(0));
                }
        } catch (Exception ex) {
            return null;
        }
    }

    static boolean isDelim(char c) { // возвращаем true если пробел
        return c == ' ';
    }

    static boolean isOperator(char c) { // возвращяем true если символ присутствует
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> st, char operation) {
        double first = st.removeLast();
        double second = st.removeLast();
        switch (operation) {
            case '+':
                st.add(second + first);
                break;
            case '-':
                st.add(second - first);
                break;
            case '*':
                st.add(second * first);
                break;
            case '/':
                st.add(second / first);
                break;
            case '%':
                st.add(second % first);
                break;
        }
    }
}
