package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if(y == null){
            throw new IllegalArgumentException("List y is null");
        }else if(x == null){
            throw new IllegalArgumentException("List x is null");
        }

        boolean sequenceIsPossible = true;
        int counter = 0;

        for(int sequenceX = 0; sequenceX < x.size(); sequenceX++){

            if(!sequenceIsPossible){
                break;
            }

            sequenceIsPossible = false;

            for(int sequenceY = counter; sequenceY <y.size(); sequenceY++){
                if(x.get(sequenceX).equals(y.get(sequenceY))){
                    sequenceIsPossible = true;
                    counter = sequenceY+1;
                    break;
                }
            }
        }

        return sequenceIsPossible;
    }
}
